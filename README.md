# Learning terraform

## Setup Provider

```
provider "aws" {
  region     = "eu-west-1"
  access_key = "anaccesskey"
  secret_key = "asecretkey"
}
```

# IAM role

```
resource "aws_iam_role" "hello_elb_iam_role" {
  name = "hello-elb-iam-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
```

* Regions list: https://www.terraform.io/docs/providers/aws/d/availability_zones.html

* Export key and secret which can be created using admin account
https://www.terraform.io/docs/providers/aws/index.html

```
$ export AWS_ACCESS_KEY_ID="anaccesskey"
$ export AWS_SECRET_ACCESS_KEY="asecretkey"
```

## Name of the regions

From this link: https://docs.aws.amazon.com/general/latest/gr/rande.html

## Images ids

From this link: https://github.com/docker/machine/issues/287

```
ap-northeast-1 ami-50eaed51
ap-southeast-1 ami-f95875ab
eu-central-1 ami-ac1524b1
eu-west-1 ami-823686f5
sa-east-1 ami-c770c1da
us-east-1 ami-4ae27e22
us-west-1 ami-d1180894
cn-north-1 ami-fe7ae8c7
us-gov-west-1 ami-cf5630ec
ap-southeast-2 ami-890b62b3
us-west-2 ami-898dd9b9
```

## Provisioner

* File copying using file provisioner

```
provisioner "file" {
  source      = "../../static/index.html"
  destination = "index.html"
}
```

 https://www.terraform.io/docs/provisioners/file.html

## Commands

* `terraform plan` to look into what will be deployed
* `terraform apply` to deploy
* `terraform destroy` destroy what has been deployed