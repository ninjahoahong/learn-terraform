variable "profile" {
  description = "Name of your profile in ~/.aws/credentials"
}

variable "region_for_deploying" {
  description = "Name of the region to deploy to"
  default = "eu-west-1"
}