provider "aws" {
  profile = "${var.profile}"
  region  = "${var.region_for_deploying}"
}

resource "aws_iam_instance_profile" "hello_elb_user" {
  name  = "hello-elb-user"
  role = "${aws_iam_role.hello_elb_iam_role.name}"
}

resource "aws_iam_role" "hello_elb_iam_role" {
  name = "hello-elb-iam-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "hello_elb_ec2_policy" {
  name = "hello-elb-ec2-policy"
  role = "${aws_iam_role.hello_elb_iam_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_elastic_beanstalk_application" "hello_elb_app" {
  name        = "hello-elb-app"
  description = "Hello Elb App"

  appversion_lifecycle {
    service_role          = "${aws_iam_role.hello_elb_iam_role.arn}"
    max_count             = 128
    delete_source_from_s3 = true
  }
}

# Beanstalk Environment
resource "aws_elastic_beanstalk_environment" "hello_elb_env" {
  name                = "hello-elb-env"
  application         = "${aws_elastic_beanstalk_application.hello_elb_app.name}"
  solution_stack_name = "64bit Amazon Linux 2018.03 v3.0.2 running Tomcat 8.5 Java 8"
}