variable "region_for_deploying" {
  description = "Name of the region to deploy to"
  default = "eu-west-1"
}

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  default = 3000
}

variable "ami_image" {
  description = "The images to build EC2 intances"
  default = "ami-823686f5"
}

provider "aws" {
  region = "${var.region_for_deploying}"
}

resource "aws_instance" "web" {
  ami = "${var.ami_image}"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.instance.id}"]
  key_name = "anh-ec2-key"

  user_data = "${file("../scripts/simple-server-start.sh")}"

  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("~/.ssh/anh-ec2-key.pem")}"
  }
  
  provisioner "file" {
    source      = "../static/index.html"
    destination = "~/index.html"
  }

  # provisioner "remote-exec" {
  #   inline = [
  #     "nohup python -m SimpleHTTPServer 3000 &> /dev/null &"
  #   ]
  # }

  # provisioner "local-exec" {
  #   command = "ansible-playbook -i ${self.public_ip} playbook.yml --limit ${self.public_ip}"
  # }

  tags {
    Name = "hello-terraform"
  }
}

resource "aws_security_group" "instance" {
  name = "hello-terraform-instance"

  ingress {
    from_port = "${var.server_port}"
    to_port = "${var.server_port}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

output "public_ip" {
  value = "${aws_instance.web.public_ip}"
}
